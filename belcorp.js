const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const ftpHost = Deno.env.get("FTP_HOST");
const user = Deno.env.get("FTP_USER");
const pass = Deno.env.get("FTP_PASS");
const articlesDirectory = Deno.env.get("FTP_ARTICLES_DIRECTORY");
const patternRegex = Deno.env.get("FTP_PATTERN");

let articleMap = {};

try {
  await processArticleFiles();
  await sagalDispatchBatch({
    products: Object.values(articleMap),
    batch_size: 100
  })
} catch (e) {
  console.log(e.message);
}

async function listFiles(folder, pattern) {
  const process = Deno.run({
    cmd: ["bash"],
    stdout: "piped",
    stdin: "piped",
  });
  try {
    const encoder = new TextEncoder();
    const decoder = new TextDecoder();

    const command = `curl  --user '${user}:${pass}' -l -k 'sftp://${ftpHost}:22${folder}' | grep '${pattern}'`;
    await process.stdin.write(encoder.encode(command));

    await process.stdin.close();
    const output = await process.output();
    return decoder
      .decode(output)
      .split(`\n`)
      .filter((x) => {
        return x != null && x != "";
      });
  } catch (e) {
    console.log(`Could not fetch files, error: ${e.message}`);
    throw e;
  } finally {
    process.close();
  }
}

async function getFile(folder, file) {
  const process = Deno.run({
    cmd: ["bash"],
    stdout: "piped",
    stdin: "piped",
  });
  try {
    const encoder = new TextEncoder();
    const decoder = new TextDecoder();
    const command = `curl  -k 'sftp://${ftpHost}:22${folder}${file}' --user '${user}:${pass}'`;
    await process.stdin.write(encoder.encode(command));
    await process.stdin.close();
    const output = await process.output();
    return decoder.decode(output);
  } catch (e) {
    console.log(`Could not fetch file: ${file}, error: ${e.message}`);
    throw e;
  } finally {
    process.close();
  }
}

async function moveFile(folder, file) {
  const process = Deno.run({
    cmd: ["bash"],
    stdout: "piped",
    stdin: "piped",
  });
  try {
    const encoder = new TextEncoder();
    const decoder = new TextDecoder();
    const command = `curl  -k 'sftp://${ftpHost}:22${folder} ' --user '${user}:${pass}' -Q "-RENAME ${folder}${file} ${folder}procesados/${file}-${new Date().toISOString()}" --ftp-create-dirs`;
    await process.stdin.write(encoder.encode(command));
    await process.stdin.close();
    const output = await process.output();
    return decoder.decode(output);
  } catch (e) {
    console.log(`Could not fetch file: ${file}, error: ${e.message}`);
    throw e;
  } finally {
    process.close();
  }
}

async function processArticleFiles() {
  console.log("Retrieving data...");
  let data = await listFiles(articlesDirectory, patternRegex);
  console.log(data);
  for (let file of data) {
    try {
      let articles = await getFile(articlesDirectory, file);

      articles = articles.split("\n").slice(1, -1);

      for (let article of articles) {
        let articleToProcess = article.split(",");
        console.log(`Processing article ${articleToProcess[0]}...`);
        await processArticle(articleToProcess);
      }
      await moveFile(articlesDirectory, file);
    } catch (e) {
      console.log(`Unable to get or process file ${file}: ${e.message}`);
    }
  }
}

async function processArticle(article) {
  let articleSku = article[0].trim().slice(1, -1);
  let articleStock = Number(article[2]);

  let processedArticle = {
    sku: articleSku,
    client_id: clientId,
    merge: false,
    integration_id: clientIntegrationId,
    ecommerce: Object.values(clientEcommerce).map((ecommerce_id) => {
      return {
        ecommerce_id: ecommerce_id,
        properties: [{ stock: articleStock }],
        variants: [],
      };
    }),
  };

  articleMap[articleSku] = processedArticle;
}
