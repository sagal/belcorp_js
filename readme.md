### Integration script for client: Belcorp

## Introduction

This script was created to run in the generic integration platform that links Sagal's clients' various forms of sending data into payloads that the business platform can understand. Belcorp is a client that mainly hosts their data in a SFTP Server that we can access to retrieve data.

## Workflow

- Article data is retrieved from the client's FTP server.
- The data is restructured and processed for each article, being transformed to a Sagal-friendly format.
- The restructured data is dispatched to Sagal.

## Environment Variables

- **CLIENT_ID**: Sagal's known identifier for the client. A number.
- **CLIENT_INTEGRATION_ID**: the integration script's id in the existing runtime environment. A number.
- **CLIENT_ECOMMERCE**: a map holding this client's known identifiers for each E-Commerce service. (e.g: Key MELI for Mercadolibre)
- **FTP_HOST**: The client's FTP server address.
- **FTP_USER**: The username required to log into the client's FTP server.
- **FTP_PASS**: The password required to log into the client's FTP server.
- **FTP_ARTICLES_DIRECTORY**: The filepath for the directory holding the client's article files.
- **FTP_PATTERN**: The pattern required to locate the article files.

## Functions

### processArticleFiles(FTPClient client)

**Description:** processes all article files that match the provided filepath pattern by calling the processArticle function for every existing article retrieved for each downloaded file.

### processArticle(Object article)

**Description:** Reestructures the article data according to Sagal's requirements, loading the processed article into an article map where articles can be identified by their sku in short order.
